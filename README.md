# sleepy-container

A silly little container that does nothing when it works. Created to
reproduce a bug in a container I can't share.
