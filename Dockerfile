FROM registry.access.redhat.com/ubi8/ubi
# Run image build steps as root
USER root
# But set an environment variable for setting config file ownership
# and entrypoint user
ENV SRVUSR=sleepy
ENV SRVGRP=sleepy
LABEL maintainer="Me <me@employer.org>"
LABEL description="A trivial Dockerfile to reproduce failure to add service users in rootless buildah/podman"
LABEL io.k8s.description="A trivial Dockerfile to reproduce failure to add service users in rootless buildah/podman"
LABEL io.k8s.dispaly-name="User fail on RHEL UBI 8"
# Define suitable yum options to avoid non-redistributable packages
ENV YUM="yum -y --disablerepo=rhel*"
 
#######################
# Set up the software #
#######################
 
ADD sleepy /usr/local/bin
ADD sleepy.conf /etc
RUN groupadd -r ${SRVGRP} || true
RUN useradd -r -g ${SRVGRP} ${SRVUSR} || true
RUN chmod +x /usr/local/bin/sleepy || true
RUN chown root:${SRVGRP} /etc/sleepy.conf ; chmod 640 /etc/sleepy.conf || true
RUN mkdir /var/local/sleepy
RUN chmod 2750 /var/local/sleepy && chown ${SRVUSR}:${SRVGRP} /var/local/sleepy || true

# Add some useful utilities for debugging
RUN ${YUM} install procps-ng iproute nmap-ncat
 
# Invoke the startup script as the service user when the image is used
USER ${SRVUSR}
ENTRYPOINT ["/usr/local/bin/sleepy"]
